$(function () {
    $(".datepicker").datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
    });
});

// Fungsi List Vaksin

var Orang = {
    nama: "",
    usia: 0,
    tanggal: "",
    warga: "",
    jenis: "",
    catatan: ""
};
var i = 1;

function display_table() {
    document.getElementById("divTable").style.display = "block";

    // Gathering the data after submit
    Orang.nama = document.getElementById("nama").value;
    Orang.usia = document.getElementById("usia").value;
    Orang.warga = document.getElementById("warga").value;
    Orang.tanggal = document.getElementById("tanggal").value;
    Orang.jenis = document.getElementById("jenis").value;
    Orang.catatan = document.getElementById("catatan").value;

    document.getElementById("nama").value = "";
    document.getElementById("usia").value = "";
    document.getElementById("warga").value = "";
    document.getElementById("tanggal").value = "";
    document.getElementById("jenis").value = "";
    document.getElementById("catatan").value = "";

    // Insert row
    var table = document.getElementById("table");
    var row = table.insertRow(i);
    var number = row.insertCell(0);
    var nama = row.insertCell(1);
    var usia = row.insertCell(2);
    var tanggal = row.insertCell(3);
    var jenis = row.insertCell(4);
    var catatan = row.insertCell(5);

    number.innerHTML = i;
    nama.innerHTML = Orang.nama;
    usia.innerHTML = Orang.usia;
    tanggal.innerHTML = Orang.tanggal;
    jenis.innerHTML = Orang.jenis;
    catatan.innerHTML = Orang.catatan;

    i++;
}

// $(document).ready(function () {
//     $('#table').DataTable();
// });
